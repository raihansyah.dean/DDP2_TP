public class TrainCar {

    public static final double EMPTY_WEIGHT = 20; // In kilograms

    public WildCat cat;
    public TrainCar next;

    public TrainCar(WildCat cat) {
        this.cat = cat;
    }

    public TrainCar(WildCat cat, TrainCar next) {
        this.cat = cat;
        this.next = next;
    }

    public double computeTotalWeight() {
        if(next == null) return (this.cat.weight + EMPTY_WEIGHT);
        return (this.cat.weight + next.computeTotalWeight() + EMPTY_WEIGHT);
    }

    public double computeTotalMassIndex() {
        if(next == null) return (this.cat.computeMassIndex());
        return (this.cat.computeMassIndex() + next.computeTotalMassIndex());
    }

    public void printCar() {
        if(next == null) System.out.println("(" + this.cat.name + ")");
        else{
            System.out.print("(" + this.cat.name + ")--");
            next.printCar();
        }
    }
}
