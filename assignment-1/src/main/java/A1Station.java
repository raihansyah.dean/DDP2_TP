import java.util.Scanner;

public class A1Station {

    private static final double THRESHOLD = 250; // in kilograms

    public static void depart(TrainCar last, int x){
        System.out.println("The train departs to Javari Park");
        System.out.print("[LOCO]<--");
        last.printCar(); // Prints names of every cat in the train

        System.out.print("Average mass index of all cats: ");

        double totalBmi = last.computeTotalMassIndex();
        double avgBmi = totalBmi/(x); // Calculating average BMI
        System.out.println(String.format("%.2f", avgBmi));

        // Category of Train based on BMI
        System.out.print("In average, the cats in the train are ");
        if (avgBmi < 18.5) System.out.print("*underweight*\n");
        else if (avgBmi < 25) System.out.print("*normal*\n");
        else if (avgBmi < 30) System.out.print("*overweight*\n");
        else System.out.print("*obese*\n");
    }

    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);

        int n = Integer.parseInt(input.nextLine());
        TrainCar now,temp=null; // Variables too keep the traincars and update them

        int j=0; // counter

        for (int i=0; i<n; i++) {

            String[] baris = input.nextLine().split(","); //Format: Name,Weight,Length
            WildCat cat = new WildCat(baris[0], Double.parseDouble(baris[1]), Double.parseDouble(baris[2]));

            if (j==0){
                temp = new TrainCar(cat);
                now = temp;
            }
            else{
                now = new TrainCar(cat, temp);
                temp = now;
            }

            if ((now.computeTotalWeight() > THRESHOLD)||(i==n-1)){
                depart(now, j+1);
                j=0;
                //train = new TrainCar[n]; // Renew array train
            }
            else j++;

        }
    }
}
