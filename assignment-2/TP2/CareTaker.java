import Animal.Cat;
import Animal.Eagle;
import Animal.Hamster;
import Animal.Lion;
import Animal.Parrot;

public class CareTaker {
    /* methods to connect javari park class with every animals behaviour and sounds */
    public static void signal() {
        System.out.println("Back to the office!");
    }

    public static void askName(String type) {
        System.out.printf("Mention the name of %s you want to visit: ", type);
    }

    public static void noFound(String type) {
        System.out.printf("There is no %s with that name! ", type);
    }

    public static void visiting(String name, String type) {
        System.out.printf("You are visiting %s (%s) now, what would you like to do?\n",
                name, type);
    }

    public static void doNothing() {
        System.out.println("You do nothing...");
    }

    public static void makeVoice(String name, String voice) {
        System.out.printf("%s makes a voice: %s\n", name, voice);
    }

    public static void brushCat(Cat cat) {
        System.out.printf("Time to clean %s's fur\n", cat.getName());
        makeVoice(cat.getName(), cat.brushed());
    }

    public static void cuddleCat(Cat cat) {
        makeVoice(cat.getName(), cat.cuddled());
    }

    public static void orderToFly(Eagle eagle) {
        makeVoice(eagle.getName(), eagle.flying());
        System.out.println("You hurt!");
    }

    public static void seeHamsterGnawing(Hamster hamster) {
        makeVoice(hamster.getName(), hamster.gnaw());
    }

    public static void orderToRun(Hamster hamster) {
        makeVoice(hamster.getName(), hamster.runOnWheel());
    }

    public static void orderToFly(Parrot parrot) {
        makeVoice(parrot.getName(), parrot.flying());
    }

    public static void talkToParrot(Parrot parrot, String word) {
        System.out.printf("%s says: %s%n", parrot.getName(), parrot.talk(word));
    }

    public static void seeLionHunt(Lion lion) {
        System.out.println("Lion is hunting..");
        makeVoice(lion.getName(), lion.hunt());
    }

    public static void brushLionMane(Lion lion) {
        System.out.printf("Clean the lion's mane..\n");
        makeVoice(lion.getName(), lion.brush());
    }

    public static void disturbLion(Lion lion) {
        makeVoice(lion.getName(), lion.disturb());
    }
}
