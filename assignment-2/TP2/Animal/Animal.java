package Animal;

public class Animal {

    protected String name;
    protected int length;

    public Animal(String nama, int length) {
        this.name = nama;
        this.length = length;
    }

    public String getName() {
        return name;
    }

    public int getLength() {
        return length;
    }
}

