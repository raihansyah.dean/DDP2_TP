package Animal;

import java.util.Random;

public class Cat extends Animal {
    private String[] voices = {"Miaaaw..", "Purrr..", "Mwaw!", "Mraaawr!"};
    public static Random random = new Random();

    public Cat(String nama, int length) {
        super(nama, length);
    }

    public String cuddled() {
        int voiceIndex = random.nextInt(4);
        return this.voices[voiceIndex];
    }

    public String brushed() {
        return "Nyaaan...";
    }
}
