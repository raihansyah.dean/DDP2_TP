package Animal;

public class Lion extends Animal {

    public Lion(String name, int length) {
        super(name, length);
    }

    public String hunt() {
        return "err...!";
    }

    public String brush() {
        return "Hauhhmm!";
    }

    public String disturb() {
        return "HAUHHMM!!";
    }
}
