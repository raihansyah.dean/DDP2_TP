package Animal;

public class Parrot extends Animal {

    public Parrot(String name, int length) {
        super(name, length);
    }

    public String flying() {
        return "FLYYYY.....";
    }

    public String talk(String word) {
        return word.toUpperCase(); // parrot can copy people's word
    }
}
