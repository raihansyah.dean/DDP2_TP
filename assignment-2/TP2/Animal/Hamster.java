package Animal;

public class Hamster extends Animal {

    public Hamster(String name, int length) {
        super(name, length);
    }

    public String gnaw() {
        return "ngkkrit.. ngkkrrriiit";
    }

    public String runOnWheel() {
        return "trrr....trrr....";
    }
}
