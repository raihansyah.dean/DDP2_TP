import Animal.Cat;
import Animal.Eagle;
import Animal.Hamster;
import Animal.Lion;
import Animal.Parrot;
import Kandang.Cage;
import Kandang.CageArrangement;

import java.util.ArrayList;
import java.util.Scanner;

public class JavariPark {

    static Cat cat;
    static Hamster hamster;
    static Parrot parrot;
    static Eagle eagle;
    static Lion lion;
    static Cage[] cats;
    static Cage[] lions;
    static Cage[] eagles;
    static Cage[] parrots;
    static Cage[] hamsters;
    static Cage[][] animals = new Cage[5][]; // for arrangement of cages
    static Scanner scan = new Scanner(System.in);
    static String[] types = {"cat", "lion", "eagle", "parrot", "hamster"};

    public static void main(String[] args) {
        System.out.println("Welcome to Javari Park!");
        animalInput();
        System.out.println("=============================================");
        cageArrangement();
        System.out.println("=============================================");
        while (javariTour()) {
            CareTaker.signal();
            System.out.println();
        }
    }


    /* for animals input */
    private static void animalInput() {
        System.out.println("Input the number of animals");
        String[] input;
        String[] temp;

        for (int i = 0; i < types.length; i++) { // input each types of animal
            String type = types[i];
            System.out.printf("%s: ", type);
            animals[i] = new Cage[scan.nextInt()];
            if (type.equals("cat")) { //array of one type of animals
                cats = animals[i];
            } else if (type.equals("lion")) {
                lions = animals[i];
            } else if (type.equals("eagle")) {
                eagles = animals[i];
            } else if (type.equals("parrot")) {
                parrots = animals[i];
            } else {
                hamsters = animals[i];
            }
            scan.nextLine();
            if (animals[i].length > 0) { // if number of 0, do nothing
                System.out.printf("Provide the information of %s(s):\n", type);
                input = scan.nextLine().split(",");
                for (int j = 0; j < animals[i].length; j++) { // loop for assign animal to the cage
                    temp = input[j].replace("|", " ").split(" ");
                    if (type.equals("cat")) {
                        cat = new Cat(temp[0], Integer.parseInt(temp[1]));
                        cats[j] = new Cage(cat);
                        animals[i][j] = cats[j];
                    } else if (type.equals("lion")) {
                        lion = new Lion(temp[0], Integer.parseInt(temp[1]));
                        lions[j] = new Cage(lion);
                        animals[i][j] = lions[j];
                    } else if (type.equals("eagle")) {
                        eagle = new Eagle(temp[0], Integer.parseInt(temp[1]));
                        eagles[j] = new Cage(eagle);
                        animals[i][j] = eagles[j];
                    } else if (type.equals("parrot")) {
                        parrot = new Parrot(temp[0], Integer.parseInt(temp[1]));
                        parrots[j] = new Cage(parrot);
                        animals[i][j] = parrots[j];
                    } else {
                        hamster = new Hamster(temp[0], Integer.parseInt(temp[1]));
                        hamsters[j] = new Cage(hamster);
                        animals[i][j] = hamsters[j];
                    }
                }
            }
        }
        System.out.println("Animals has been successfully recorded!");
    }

    /* method for manage the arrangement of cages */
    private static void cageArrangement() {
        System.out.println("Cage arrangement:");
        for (int i = 0; i < animals.length; i++) {
            if (animals[i].length > 0) {
                System.out.printf("location: %s\n", animals[i][0].getLocation());
                ArrayList<Cage>[] level = CageArrangement.arrangeCage(animals[i]);
                System.out.println("After rearrangement...");
                CageArrangement.rearrangement(level);
            }
        }
        System.out.println("\nANIMALS NUMBER:"); // print number of each animals type
        for (int i = 0; i < types.length; i++) {
            System.out.printf("%s\t: %d\n", types[i], animals[i].length);
        }
        System.out.println();
    }

    /* method for visiting animals in javari park */
    public static boolean javariTour() {
        System.out.println("Which animal you want to visit?\n"
                + "(1: Cat, 2: Eagle, 3: Hamster, 4: Parrot, 5: Lion, 99: exit)");

        int input = scan.nextInt();
        if (input == 99) { // if input 99, stop looping
            System.out.println("Thank you for visiting Javari Park!");
            return false;
        } else { // decide which animal that want to visit, each animal have different behavior
            int temp;
            scan.nextLine();
            if (input == 1) {
                CareTaker.askName("cat");
                Cat cat = findCat(scan.nextLine());
                if (cat != null) {
                    CareTaker.visiting(cat.getName(), "cat");
                    System.out.println("1: Brush the fur 2: Cuddle");
                    temp = scan.nextInt();
                    if (temp == 1) {
                        CareTaker.brushCat(cat);
                    } else if (temp == 2) {
                        CareTaker.cuddleCat(cat);
                    } else {
                        CareTaker.doNothing();
                    }
                } else {
                    CareTaker.noFound("cat");
                }
            } else if (input == 2) {
                CareTaker.askName("eagle");
                Eagle eagle = findEagle(scan.nextLine());
                if (eagle != null) {
                    CareTaker.visiting(eagle.getName(), "eagle");
                    System.out.println("1: Order to fly");
                    if (scan.nextInt() == 1) {
                        CareTaker.orderToFly(eagle);
                    } else {
                        CareTaker.doNothing();
                    }
                } else {
                    CareTaker.noFound("eagle");
                }
            } else if (input == 3) {
                CareTaker.askName("hamster");
                Hamster hamster = findHamster(scan.nextLine());
                if (hamster != null) {
                    CareTaker.visiting(hamster.getName(), "hamster");
                    System.out.println("1: See it gnawing 2: Order to run in the hamster wheel");
                    temp = scan.nextInt();
                    if (temp == 1) {
                        CareTaker.seeHamsterGnawing(hamster);
                    } else if (temp == 2) {
                        CareTaker.orderToRun(hamster);
                    } else {
                        CareTaker.doNothing();
                    }
                } else {
                    CareTaker.noFound("hamster");
                }
            } else if (input == 4) {
                CareTaker.askName("parrot");
                Parrot parrot = findParrot(scan.nextLine());
                if (parrot != null) {
                    CareTaker.visiting(parrot.getName(), "parrot");
                    System.out.println("1: Order to fly 2: Do conversation");
                    temp = scan.nextInt();
                    if (temp == 1) {
                        CareTaker.orderToFly(parrot);
                    } else if (temp == 2) {
                        scan.nextLine();
                        System.out.print("You say: ");
                        String word = scan.nextLine();
                        CareTaker.talkToParrot(parrot, word);
                    } else {
                        CareTaker.talkToParrot(parrot, "hm?");
                    }
                } else {
                    CareTaker.noFound("parrot");
                }
            } else if (input == 5) {
                CareTaker.askName("Lion");
                Lion lion = findLion(scan.nextLine());
                if (lion != null) {
                    CareTaker.visiting(lion.getName(), "lion");
                    System.out.println("1: see it hunting 2: Brush the mane 3: Disturb it");
                    temp = scan.nextInt();
                    if (temp == 1) {
                        CareTaker.seeLionHunt(lion);
                    } else if (temp == 2) {
                        CareTaker.brushLionMane(lion);
                    } else if (temp == 3) {
                        CareTaker.disturbLion(lion);
                    } else {
                        CareTaker.doNothing();
                    }
                } else {
                    CareTaker.noFound("lion");
                }
            } else {
                CareTaker.doNothing();
            }
        }
        return true;
    }

    /* method to find the animal that user search for, return null if doesnt exist*/
    public static Cat findCat(String name) {
        for (int i = 0; i < cats.length; i++) {
            if (cats[i].getCat().getName().equals(name)) {
                return cats[i].getCat();
            }
        }
        return null;
    }

    public static Eagle findEagle(String name) {
        for (int i = 0; i < eagles.length; i++) {
            if (eagles[i].getEagle().getName().equals(name)) {
                return eagles[i].getEagle();
            }
        }
        return null;
    }

    public static Parrot findParrot(String name) {
        for (int i = 0; i < parrots.length; i++) {
            if (parrots[i].getParrot().getName().equals(name)) {
                return parrots[i].getParrot();
            }
        }
        return null;
    }

    public static Hamster findHamster(String name) {
        for (int i = 0; i < hamsters.length; i++) {
            if (hamsters[i].getHamster().getName().equals(name)) {
                return hamsters[i].getHamster();
            }
        }
        return null;
    }

    public static Lion findLion(String name) {
        for (int i = 0; i < lions.length; i++) {
            if (lions[i].getLion().getName().equals(name)) {
                return lions[i].getLion();
            }
        }
        return null;
    }
}
