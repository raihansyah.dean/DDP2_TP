package Kandang;

import java.util.ArrayList;

public class CageArrangement {

    public static final int LEVEL = 3;

    /* method for splitting cage into 3 level */
    public static ArrayList<Cage>[] arrangeCage(Cage[] cages) {
        ArrayList<Cage>[] level = new ArrayList[LEVEL];
        for (int i = 0; i < LEVEL; i++) {
            level[i] = new ArrayList<Cage>(); // initialize arraylist of each index
        }

        if (cages.length < LEVEL) { // handle if length less than 3
            for (int i = 0; i < cages.length; i++) {
                level[i].add(cages[i]);
            }
        } else {
            int levelCapacity = cages.length / LEVEL; //limit per level
            for (int i = 0; i < cages.length; i++) {
                if (i / levelCapacity >= 3) { // if not 0 % 3
                    level[2].add(cages[i]);
                    continue;
                }
                level[i / levelCapacity].add(cages[i]); //insert to the same level
            }
        }
        printCages(level);
        return level;
    }

    /* method to reverse cages in each level and change its level */
    public static void rearrangement(ArrayList<Cage>[] level) {
        ArrayList<Cage>[] newLevel = new ArrayList[LEVEL];
        for (int i = 0; i < LEVEL; i++) {
            newLevel[i] = new ArrayList<Cage>();
            for (int j = level[i].size() - 1; j >= 0; j--) { // reverse the cage
                newLevel[i].add(level[i].get(j));
            }
        }
        ArrayList<Cage> temp = newLevel[2];
        for (int i = LEVEL - 1; i > 0; i--) { // change the level of cage
            newLevel[i] = newLevel[i - 1];
        }
        newLevel[0] = temp;
        printCages(newLevel);
    }

    /* method to print the current block of cages */
    public static void printCages(ArrayList<Cage>[] level) {
        for (int i = LEVEL; i > 0; i--) {
            System.out.printf("level %d:", i);
            for (int j = 0; j < level[i - 1].size(); j++) {
                System.out.printf(" %s,", level[i - 1].get(j).getInfo());
            }
            System.out.println();
        }
        System.out.println();
    }
}
