package Kandang;

import Animal.Animal;
import Animal.Cat;
import Animal.Hamster;
import Animal.Parrot;
import Animal.Eagle;
import Animal.Lion;

public class Cage {

    /* this class contain one of these animal */
    private Cat cat;
    private Hamster hamster;
    private Parrot parrot;
    private Eagle eagle;
    private Lion lion;

    private boolean isOutdoor; // true if wild animal
    private String type; // there are 3 types of cages
    private String cageInfo;

    public Cage(Cat cat) {
        this.cat = cat;
        this.isOutdoor = false;
        setType(cat);
        setInfo(cat);
    }

    public Cage(Hamster hamster) {
        this.hamster = hamster;
        this.isOutdoor = false;
        setType(hamster);
        setInfo(hamster);
    }

    public Cage(Parrot parrot) {
        this.parrot = parrot;
        this.isOutdoor = false;
        setType(parrot);
        setInfo(parrot);
    }

    public Cage(Eagle eagle) {
        this.eagle = eagle;
        this.isOutdoor = true;
        setType(eagle);
        setInfo(eagle);
    }

    public Cage(Lion lion) {
        this.lion = lion;
        this.isOutdoor = true;
        setType(lion);
        setInfo(lion);
    }


    /* setters & getters */
    public String getLocation() {
        if (isOutdoor) {
            return "outdoor";
        } else {
            return "indoor";
        }
    }

    public void setType(Animal animal) { // to decide the suitable cage
        int animalLength = animal.getLength();
        if ((isOutdoor && animalLength < 75) || (!isOutdoor && animalLength < 45)) {
            type = "A";
        } else if ((isOutdoor && animalLength <= 90) || (!isOutdoor && animalLength < 60)) {
            type = "B";
        } else {
            type = "C";
        }
    }

    public Cat getCat() {
        return cat;
    }

    public Hamster getHamster() {
        return hamster;
    }

    public Parrot getParrot() {
        return parrot;
    }

    public Eagle getEagle() {
        return eagle;
    }

    public Lion getLion() {
        return lion;
    }

    public String getType() {
        return type;
    }


    /* information about animal's name & length, and cage's type */
    public void setInfo(Animal animal) {
        cageInfo = String.format("%s (%d - %s)", animal.getName(), animal.getLength(), type);
    }

    public String getInfo() {
        return cageInfo;
    }
}
