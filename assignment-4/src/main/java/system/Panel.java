package system;

import system.card.Card;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Collections;

/**
 * This class represents the main GUI Panel of the game card.
 *
 * @author Raihansyah Attallah Andrian
 */
public class Panel extends JFrame{

    /**
     * Reset Button.
     */
    private JButton reset;

    /**
     * Label for number of tries.
     */
    private JLabel tries;

    /**
     * Containers.
     * Pane for the main content pane.
     * cardPane for the container for cards.
     * buttonPane for the container for buttons.
     *
     */
    private Container pane, cardPane, buttonPane;

    /**
     * An arraylist of cards to store the cards that will be presented in the main panel.
     *
     */
    private ArrayList<Card> cards;

    /**
     * An array list of ImageIcons that is used as the parameters to create cards.
     */
    private ArrayList<ImageIcon> cardImages;


    /**
     * Constructs an instance of {@code Panel}.
     *
     * Calls the method {@code initGui()}.
     */
    public Panel(){
        this.initGui();
    }

    /**
     * A method to initiate the GUI and start the main GUI Panel.
     *
     * Calls the method {@code setGame()}
     */
    private void initGui(){
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        //Calling the main panel of the GUI that is used to hold objects, named "Content Pane".
        pane = getContentPane();

        //The Panel for Cards.
        cardPane = new JPanel();
        cardPane.setLayout(new GridLayout(0,6));

        //The Panel for buttons.
        buttonPane = new JPanel();
        buttonPane.setLayout(new GridLayout(0,6));

        //Calling method.
        setGame();

        setResizable(false);
        setTitle("Memory Game - The Avengers Logos");
        pack();
        setVisible(true);
    }

    /**
     * A method to set the game at first launched.
     *
     * Calls the method {@code createImages()} and {@code createCard()}.
     */
    public void setGame(){
        //Calling method.
        cardImages = createImages();
        //Calling method.
        cards = createCards(cardImages);
        //Shuffling the cards.
        Collections.shuffle(cards);

        //Setting each cards such that at first the images are the cover.
        for(Card card : cards){
            card.setIcon(Card.COVER);
            card.setPreferredSize(new Dimension(150,150));
            cardPane.add(card);
        }

        //Creating the initial number of tries.
        tries = new JLabel("Number of tries : " + 0);

        //Creating the "Reset" button.
        reset = new JButton("Reset");
        reset.setMaximumSize(new Dimension(150,100));

        //Creating the "Exit" button.
        JButton exit = new JButton("Exit");
        exit.addActionListener(e -> System.exit(0));
        //lambda equation

        //Creating spaces for the Labels and buttons (configuring spaces)
        buttonPane.add(new JLabel());
        buttonPane.add(new JLabel());
        buttonPane.add(reset);
        buttonPane.add(exit);
        for(int i = 0; i < 4 ; i++)
            buttonPane.add(new JLabel());
        buttonPane.add(tries);
        buttonPane.setBackground(Color.lightGray);

        JSeparator separator1 = new JSeparator();
        separator1.setPreferredSize(new Dimension(900, 10));

        //Configuring the layouts for each objects.
        pane.add(cardPane, BorderLayout.NORTH);
        pane.add(separator1, BorderLayout.CENTER);
        pane.add(buttonPane, BorderLayout.SOUTH);
        pane.setBackground(Color.lightGray);
    }

    /**
     * A method to create the images at first.
     *
     * @return ArrayList of ImageIcons that is then used in {@code createCards()}.
     *         Each image are added twice (for the pairs).
     */
    public ArrayList<ImageIcon> createImages(){
        ArrayList<ImageIcon> images = new ArrayList<>();

        String path = System.getProperty("user.dir") + "/src/TP/TP4/src/main/java/system/img/hero/";

        for(int i = 1; i <= 18; i++){
            ImageIcon ii = new ImageIcon(path + i + ".png");
            images.add(ii);
            images.add(ii);
        }

        return images;
    }

    /**
     * A method to create the cards.
     *
     * @param AI ArrayList of ImageIcons that is used as parameters to create each card.
     * @return ArrayList of Cards that will be
     *         used as the cards to be presented in the GUI panel.
     */
    public ArrayList<Card> createCards(ArrayList<ImageIcon> AI){
        ArrayList<Card> cards = new ArrayList<>();

        for(ImageIcon img : AI){
            Card c = new Card(img);
            cards.add(c);
        }

        return cards;
    }

    /**
     * A method to set the function of each cards.
     *
     * @param listener an ActionListener to be assigned to each card.
     */
    public void setCardsFunction(ActionListener listener){
        for(JButton button : cards){
            button.addActionListener(listener);
        }
    }

    /**
     * A method to set the reset button ActionListener.
     *
     * @param listener an ActionListener to be assigned to the reset button.
     */
    public void setReset(ActionListener listener){
        reset.addActionListener(listener);
    }

    /**
     * A method to get the cards in this object.
     *
     * @return An ArrayList of cards {@code cards}.
     */
    public ArrayList<Card> getCards(){
        return cards;
    }

    /**
     * A method to get the tries in this object.
     *
     * @return A JLabel of the "number of tries" label {@code tries}.
     */
    public JLabel getTries(){
        return tries;
    }
}
