package system.card;

import javax.swing.*;
import java.awt.*;

/**
 * This class represents attributes and behaviours found in each of the game card.
 *
 * @author Raihansyah Attallah Andrian
 */

public class Card extends JButton{

    /**
     * Create the cover of each card from the directory of the cover (logo.png).
     *
     */
    public static final ImageIcon COVER = scaleImage(
            new ImageIcon(System.getProperty("user.dir") + "/src/TP/TP4/src/main/java/system/img/logo.png"));

    private ImageIcon image;
    private String ID;
    private boolean match = false;

    /**
     * Constructs an instance of {@code Card}.
     *
     * @param image ImageIcon object that is remake to Card object.
     */
    public Card(ImageIcon image) {
        this.image = scaleImage(image);
        this.ID = image.getDescription();
    }

    /**
     * A method to scale a previous ImageIcon object to the scaled size.
     *
     * @param image previous ImageIcon object
     * @return scaled ImageIcon object
     */
    private static ImageIcon scaleImage(ImageIcon image) {
        return new ImageIcon(image.getImage().getScaledInstance(150, 150, Image.SCALE_DEFAULT));
    }

    /**
     * Returns {@code match} of the Card.
     *
     * @return
     */
    public boolean isMatch() {
        return this.match;
    }

    /**
     * Changes Card's match attribute.
     *
     * @param match status whether the card has been matched or not.
     *              True if card has been matched.
     */
    public void setMatch(boolean match) {
        this.match = match;
    }

    /**
     * Returns {@code ID} of the Card.
     *
     * @return
     */
    public String getID() {
        return ID;
    }

    /**
     * Returns {@code image} of the Card.
     *
     * @return
     */
    public ImageIcon getImage() {
        return image;
    }
}