package system;

import system.card.Card;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * This class represents the controller of the game (the logic and timing).
 *
 * @author Raihansyah Attallah Andrian
 */
public class Controller {

    /**
     * The GUI Panel.
     */
    private Panel gui;

    /**
     * The currently picked cards.
     */
    private Card c1 = null, c2 = null;

    /**
     * A timer object to implement the timing logic.
     */
    private Timer t;

    /**
     * A counter to set the number of tries.
     */
    private int counter = 0;

    /**
     * Constructs an instance of {@code Controller}.
     *
     * Empty implementation.
     */
    public Controller() {}

    /**
     * A method to run the GUI Panel.
     *
     * Calls the method {@code createGUI()}
     */
    public void run(){
        if(gui == null){
            createGUI();
        }
        else{
            gui.dispose();
            createGUI();
        }
    }

    /**
     * A method to create the GUI Panel
     */
    public void createGUI(){
        this.gui = new Panel();

        /**
         * Calling the setCardsFunction to override
         * the builtin actionPerformed function to {@code turnCard()} function.
         */
        gui.setCardsFunction(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                turnCard((Card) e.getSource());
            }
        });

        /**
         * Calling the setReset to override
         * the builtin actionPerformed function to recreate the GUI Panel.
         */
        gui.setReset(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                counter = 0;
                run();
            }
        });

        /**
         * Calling a new Timer to override the builtin actionPerformed
         * function to check the card using {@code checkCard()} function.
         */
        t = new Timer(500, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                checkCard();
            }
        });
        t.setRepeats(false);
    }

    /**
     * A method to turn the card face up.
     *
     * @param c The current Card that we want to turn.
     */
    public void turnCard(Card c){
        if(c1 == null && c2 == null){
            c1 = c;
            c1.setIcon(c1.getImage());
        }

        if(c1 != null && c1 != c && c2 == null){
            c2 = c;
            c2.setIcon(c2.getImage());
            t.start();
        }
    }

    /**
     * A method to check the currently open cards.
     *
     * Calls the method {@code isWin()} to check if the player has won or not.
     */
    public void checkCard(){
        if(c1.getID().equals(c2.getID())){
            c1.setEnabled(false);
            c2.setEnabled(false);
            c1.setMatch(true);
            c2.setMatch(true);
            if(isWin()){
                JOptionPane.showMessageDialog(gui, "You Win! Your Score : " + (counter + 1));
            }
        }
        else{
            c1.setIcon(Card.COVER);
            c2.setIcon(Card.COVER);
        }

        //Increment the number of tries
        gui.getTries().setText("Number of tries : " + ++counter);
        c1 = null;
        c2 = null;
    }

    /**
     * A method to check the condition of winning.
     *
     * @return boolean true if the player has won the game.
     *         false otherwise.
     */
    public boolean isWin(){
        for(Card card : gui.getCards()){
            if(!card.isMatch())
                return false;
        }
        return true;
    }
}
