import system.Controller;

/**
 * This class represents the main function of the game
 * where a {@code Controller} object is created and then run the game.
 *
 * @author Raihansyah Attallah Andrian
 */
public class Game {
    public static void main(String[] args) {
        Controller controller = new Controller();
        controller.run();
    }
}
